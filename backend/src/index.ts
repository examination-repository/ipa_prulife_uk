import os from 'os';
import cluster from 'cluster';
import app from './app';

const cpuLength: number = os.cpus().length;

if (cluster.isMaster) {
    for (let i = 0; i < cpuLength; i++) {
        cluster.fork();
    }

    cluster.on('exit', () => cluster.fork());
} else {
    app();
}
