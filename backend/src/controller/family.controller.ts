import { Request, Response } from 'express';
export default class Family {
    async getFamilyMember(request: Request, response: Response) {}

    async addFamilyMember(request: Request, response: Response) {}

    async editFamilyMember(request: Request, response: Response) {}

    async deleteFamilyMember(request: Request, response: Response) {}
}
