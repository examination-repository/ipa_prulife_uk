import express, { Application } from 'express';
import cors from 'cors';
import { config } from 'dotenv';
import morgan from 'morgan';
import helmet from 'helmet';
import db from './config/db';
config();

export default function () {
    const server: Application = express();
    const PORT: any = process.env.PORT;

    server.use(express.json());
    server.use(express.urlencoded({ extended: true }));
    server.use(cors());

    server.use(helmet.xssFilter());
    server.use(helmet.hidePoweredBy());

    if (process.env.NODE_ENV === 'development') {
        server.use(morgan('dev'));
    }

    (async () => {
        try {
            const query = await db.query('SELECT 1');
            console.log(query[0]);

            server.listen(PORT, () => console.log(`server is listening on port ${PORT}`));
        } catch (error) {
            console.log(error);
        }
    })();
}

process.on('uncaughtException', () => {
    process.on('beforeExit', () => {
        console.log('unhandled exception detected, process will stop');
    });

    process.on('exit', () => {
        process.exit(1);
    });
});
