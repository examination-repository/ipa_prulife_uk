const mysql2 = require('mysql2');
import { config } from 'dotenv';

config();

const pool: any = mysql2.createPool({
    host: `${process.env.DATABASE_HOSTNAME}`,
    user: `${process.env.DATABASE_USERNAME}`,
    password: `${process.env.DATABASE_PASSWORD}`,
    database: `${process.env.DATABASE_DB}`,
    port: parseInt(`${process.env.DATABASE_PORT}`),
});

const poolPromise = pool.promise();

export default poolPromise;
